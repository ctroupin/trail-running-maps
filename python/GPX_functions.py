import re
import os
import gpxpy
import numpy as np
import matplotlib.patches as mpatches
import matplotlib.path as mpath
from geopy import distance
from pyproj import Proj, transform
from geopy.distance import great_circle
from fitparse import FitFile
import matplotlib as mpl
import matplotlib.pyplot as plt
cmap = mpl.cm.get_cmap('hot_r')
norm = mpl.colors.Normalize(vmin=15.0, vmax=20.0)

from matplotlib.font_manager import FontProperties
fa_dir = r"/home/ctroupin/.fonts/"
fp1 = FontProperties(fname=os.path.join(fa_dir, "Font Awesome 6 Free-Solid-900.otf"))
fontfile = "/home/ctroupin/.fonts/D-DIN.ttf"
fontfile = "/home/ctroupin/.fonts/nasalization-rg.otf"
myfont = FontProperties(fname=fontfile)

def get_activity(datafile):
    fitfile = FitFile(datafile)
    activitytype = ""
    for ii, record in enumerate(fitfile.get_messages('record')):
        for rr in record.fields:
            if "activity" in rr.name:
                activitytype = record.get_value("activity_type")
                break
        break

    return activitytype, ii
        

def read_gpx_regex(filename):
    lon, lat, elevation = [], [], []
    with open(filename) as f:
        for lines in f:
            match = re.search('<trkpt lat="([-0-9\.]+)" lon="([-0-9\.]+)">', lines)
            if match:
                lat.append(float(match.group(1)))
                lon.append(float(match.group(2)))
            else:
                matchele = re.search("<ele>([-0-9\.]+)</ele>", lines)
                if matchele:
                    elevation.append(float(matchele.group(1)))

    return lon, lat, elevation

def read_gpx(filename):
    """
    Read the coordinates, the elevation and the time from a GPX file
    """
    lon, lat, ele, t = [], [], [], []
    with open(filename, 'r') as f:
        gpx_parser = gpxpy.parse(f)

        for track in gpx_parser.tracks:
            for segment in track.segments:
                for point in segment.points:
                    if point.time is not None:
                        t.append(point.time)
                        lat.append(point.latitude)
                        lon.append(point.longitude)
                        ele.append(point.elevation)
                    else:
                        break

    # Need to ensure the time instances are unique
    t = np.array(t)
    t_u, ind = np.unique(t, return_index=True)
    lon = np.array(lon)[ind]
    lat = np.array(lat)[ind]
    ele = np.array(ele)[ind]

    return lon, lat, ele, t_u

def read_gpx_latlon(filename):
    """
    Read the coordinates only from a GPX file
    """
    lon, lat = [], []
    with open(filename, 'r') as f:
        gpx_parser = gpxpy.parse(f)
        for track in gpx_parser.tracks:
            for segment in track.segments:
                for point in segment.points:
                    lat.append(point.latitude)
                    lon.append(point.longitude)

    return lon, lat

def read_all_coords(datafilelist):
    """Read all the coordinates from the files listed in `datafilelist`
    """
    lon_all, lat_all, ele_all = np.array([]), np.array([]), np.array([])
    for datafile in datafilelist:
        lon, lat, ele, t = read_gpx(datafile)


        lon_all = np.append(lon_all, lon)
        lat_all = np.append(lat_all, lat)
        ele_all = np.append(ele_all, ele)
        lon_all = np.append(lon_all, np.nan)
        lat_all = np.append(lat_all, np.nan)
        ele_all = np.append(ele_all, np.nan)

    return lon_all, lat_all, ele_all

def read_data_fit(datafile):
    fitfile = FitFile(datafile)
    T = []
    lon = []
    lat = []
    ele = []
    hr = []
    times = []
    # Get all data messages that are of type record
    for record in fitfile.get_messages('record'):
        for rr in record:
            if rr.name == "position_lat":
                lat.append(rr.value)
            if rr.name == "position_long":
                lon.append(rr.value)
            if rr.name == "temperature":
                T.append(rr.value)
            if rr.name == "heart_rate":
                hr.append(rr.value)
            if rr.name == "timestamp":
                times.append(rr.value)
            if rr.name == "enhanced_altitude":
                ele.append(rr.value)
                
    lon = np.array(lon[1:]) / 11930465
    lat = np.array(lat[1:]) / 11930465
    hr = np.array(hr)
    T = np.array(T)
    ele = np.array(ele)
    return lon, lat, ele, times, T, hr

def read_all_coords_ori(datafilelist, origin=None, dmax=10.):
    """
    Read all the coordinates from the files listed in `datafilelist`

    If `origin` and `dmax` are defined, only the tracks with the mean position
    closer than `dmax` (km) from origin are kept.
    """
    lon_all, lat_all, ele_all = np.array([]), np.array([]), np.array([])
    for datafile in datafilelist:
        lon, lat, ele, t = read_gpx(datafile)

        if origin is not None:
            d = great_circle((origin[0], (origin[1])),
                     (np.array(lat).mean(), np.array(lon).mean())).km
        else:
            d = 0.

        if d < dmax:

            lon_all = np.append(lon_all, lon)
            lat_all = np.append(lat_all, lat)
            ele_all = np.append(ele_all, ele)
            lon_all = np.append(lon_all, np.nan)
            lat_all = np.append(lat_all, np.nan)
            ele_all = np.append(ele_all, np.nan)

    return lon_all, lat_all, ele_all

def compute_dist(lon, lat):
    """
    Compute the distance between successive positions
    """
    dist = np.zeros_like(lon)
    for i in range(1, len(lon)):
        dist[i] = distance.geodesic((lat[i],lon[i]), (lat[i-1], lon[i-1])).kilometers

    return dist

def runningmean(x, N):
        return np.convolve(x, np.ones((N,))/N)[(N-1):]

def compute_speed(lon, lat, time):
    """
    Compute the speed in km/h, using the Vicenty formula for the distance
    """

    speed = np.zeros_like(lon)
    for i in range(0, len(lon)-1):
        # Compute distance between consecutive points
        d = distance.geodesic((lat[i+1],lon[i+1]), (lat[i], lon[i])).kilometers
        dt = (time[i+1] - time[i]).total_seconds()
        speed[i] = d / dt
    speed *= 3600.
    return speed

def compute_mean_pos(filename):
    """
    Compute the mean position
    """
    lon, lat, ele, time = read_gpx(filename)
    lon = np.array(lon)
    lat = np.array(lat)
    lonmean = lon.mean()
    latmean = lat.mean()
    return lonmean, latmean

def get_index_km(lon, lat):
    """
    Get the indices of the coordinates that correspond to km 1, km 2, ... km N
    """
    dist = compute_dist(lat, lon)
    cumdist = np.cumsum(dist)
    ind = []
    for dd in np.arange(1, cumdist[-1]):
        ind.append(np.argmin(abs(cumdist - dd)))

    return ind

def get_scale_pos(lon, lat):
    """
    Compute the position of the map scale
    """
    lonmin = lon.min()
    lonmax = lon.max()
    latmin = lat.min()
    latmax = lat.max()

    lonscale = lonmin + 0.8 * (lonmax - lonmin)
    latscale = latmin + 0.75 * (latmax - latmin)

    return lonscale, latscale

def get_domain_(lon, lat, r=0.05):
    """
    Compute the domain extent based on the list of longitudes and latitudes
    """
    lonmin = lon.min()
    lonmax = lon.max()
    latmin = lat.min()
    latmax = lat.max()
    deltalon = lonmax - lonmin
    deltalat = latmax - latmin

    domain = [lonmin - r * deltalon, lonmax + r * deltalon,
              latmin - r * deltalat, latmax + r * deltalat]

    return domain

def mask_outside_polygon(lons, lats, ax=None, zorder=2, alpha=1, revert=False,
                         fc="k", ec='#137038'):
    """
    Plots a mask on the specified axis ("ax", defaults to plt.gca()) such that
    all areas outside of the polygon specified by "poly_verts" are masked.

    "poly_verts" must be a list of tuples of the verticies in the polygon in
    counter-clockwise order.

    Returns the matplotlib.patches.PathPatch instance plotted on the figure.
    """

    if revert is True:
        poly_verts = [(xx, yy) for xx,yy in zip(lons[-1:0:-1], lats[-1:0:-1])]
    else:
        poly_verts = [(xx, yy) for xx,yy in zip(lons[:], lats[:])]
    if ax is None:
        ax = plt.gca()

    # Get current plot limits
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()

    # Verticies of the plot boundaries in clockwise order
    bound_verts = [(xlim[0], ylim[0]), (xlim[0], ylim[1]),
                   (xlim[1], ylim[1]), (xlim[1], ylim[0]),
                   (xlim[0], ylim[0])]

    # A series of codes (1 and 2) to tell matplotlib whether to draw a line or
    # move the "pen" (So that there's no connecting line)
    bound_codes = [mpath.Path.MOVETO] + (len(bound_verts) - 1) * [mpath.Path.LINETO]
    poly_codes = [mpath.Path.MOVETO] + (len(poly_verts) - 1) * [mpath.Path.LINETO]

    # Plot the masking patch
    path = mpath.Path(bound_verts + poly_verts, bound_codes + poly_codes)
    patch = mpatches.PathPatch(path, facecolor=fc, edgecolor=ec, linewidth=2, zorder=zorder)
    patch = ax.add_patch(patch)


def speed_plot(lon, lat, time, speed, cmap=plt.cm.hot_r, domain=[], figname="", plottype="scatter",
               vmin=16., vmax=22., figtitle=""):
    """Create a plot of the velocity computed from the positions

    """

    fig = plt.figure(figsize=(10, 9))
    ax = plt.subplot(111)

    if len(figtitle) > 0:
        plt.title(figtitle, fontsize=28, fontproperties=myfont)

    if len(domain) == 4:
        plt.xlim(domain[0], domain[1])
        plt.ylim(domain[2], domain[3])

    plt.plot(lon, lat, ".9", linewidth=.2, alpha=.5, zorder=3)
    norm = mpl.colors.BoundaryNorm(np.arange(vmin, vmax + 0.00001, 1), cmap.N)

    if plottype == "scatter":
        m = plt.scatter(lon, lat, c=speed, s=15, cmap=cmap, norm=norm, zorder=4)
    elif plottype == "hexbin":
        m = plt.hexbin(lon, lat, C=speed, gridsize=200, cmap=cmap, norm=norm, zorder=4)
    elif plottype == "arrow":
        i = 0
        for i in range(0, len(lon)-1):
            if speed[i] > vmin:
                m = plt.scatter(lon[10:100], lat[10:100], c=speed[10:100], s=0.5, cmap=cmap, norm=norm, zorder=4)
                plt.arrow(lon[i], lat[i], lon[i+1]-lon[i], lat[i+1]-lat[i], color=cmap(norm(speed[i])),
                          alpha=0.5, width=0.000001, head_width=0.00001, zorder=4)

    cbaxes = fig.add_axes([0.6, 0.185, 0.275, 0.025])
    cb = plt.colorbar(m, cax=cbaxes, orientation="horizontal", extend="both")
    cb.set_label("km/h", fontproperties=myfont)
    for t in cb.ax.get_xticklabels():
        t.set_font_properties(myfont)

    ax.set_facecolor('.2')
    ax.set_xticks([])
    ax.set_yticks([])
    plt.text(0.02, 0.95, time[0].strftime("%Y-%m-%d"),
             fontsize=24, fontproperties=myfont,
             transform=ax.transAxes)



    if len(figname) > 0:
        plt.savefig(figname, dpi=300, bbox_inches="tight")
    # plt.show()
    plt.close()

def hr_plot(lon, lat, time, hr, cmap=plt.cm.hot_r, domain=[], figname="", 
            hrmin=120., hrmax=180, figtitle=""):
    """Create a plot of the hearth rate 

    """

    fig = plt.figure(figsize=(10, 9))
    ax = plt.subplot(111)

    if len(figtitle) > 0:
        plt.title(figtitle, fontsize=28, fontproperties=myfont)

    if len(domain) == 4:
        plt.xlim(domain[0], domain[1])
        plt.ylim(domain[2], domain[3])

    plt.plot(lon, lat, ".9", linewidth=.2, alpha=.5, zorder=3)
    norm = mpl.colors.BoundaryNorm(np.arange(hrmin, hrmax + 0.00001, 5), cmap.N)

    m = plt.scatter(lon[0], lat[0], c=hr[0], s=15, cmap=cmap, norm=norm)

    for i in range(0, len(lon)-1):
        if hr[i] > hrmin:
            plt.arrow(lon[i], lat[i], lon[i+1]-lon[i], lat[i+1]-lat[i], color=cmap(norm(hr[i])),
                      alpha=0.5, width=0.000001, head_width=0.00001, zorder=4)

    cbaxes = fig.add_axes([0.6, 0.185, 0.275, 0.025])
    cb = plt.colorbar(m, cax=cbaxes, orientation="horizontal", extend="both")
    cb.set_label("BPM", fontproperties=myfont)
    for t in cb.ax.get_xticklabels():
        t.set_font_properties(myfont)

    ax.set_facecolor('.2')
    ax.set_xticks([])
    ax.set_yticks([])
    plt.text(0.02, 0.95, time[0].strftime("%Y-%m-%d"),
             fontsize=24, fontproperties=myfont,
             transform=ax.transAxes)

    if len(figname) > 0:
        plt.savefig(figname, dpi=300, bbox_inches="tight")
    plt.show()
    plt.close()
