#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import matplotlib.pyplot as plt
import cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import numpy as np
import gpxpy
from pyproj import Transformer
from pyproj import CRS
import GPX_functions
import cartopy.mpl.gridliner as gridliner
import matplotlib.ticker as mticker
import cartopy.mpl.ticker as cartopyticker
lon_formatter = cartopyticker.LongitudeFormatter()
lat_formatter = cartopyticker.LatitudeFormatter()
plt.rcParams.update({'font.size': 12})
figdir = "../images/"


# In[2]:


gpxfile = "/data/GPX/GranCanaria/BarrancoNegro_2017_12_28_10_27_37_Trekking.gpx"
lon, lat, ele, t = GPX_functions.read_gpx(gpxfile)
coordinates = [lon.min(), lon.max(), lat.min(), lat.max()]


# In[3]:


# Start projection
myproj = cartopy.crs.Mercator(central_longitude=0.5 * (coordinates[0] + coordinates[1]), 
                     min_latitude=coordinates[2], max_latitude=coordinates[3], 
                     globe=None, latitude_true_scale=None)


# In[ ]:


fig = plt.figure(figsize=(12, 12))
ax = plt.axes(projection=myproj)

#ax.coastlines()
scat = ax.scatter(lon, lat, s=6, c=ele, 
                  cmap=plt.cm.inferno_r, zorder=5,
                  transform=ccrs.PlateCarree())

cb = plt.colorbar(scat, extend="max", shrink=.5, pad=0.02)
cb.set_label("[m]", rotation=0, ha="left", labelpad=-12.5)

ax.add_wms(wms='http://ows.emodnet-bathymetry.eu/wms',
               layers=['emodnet:mean_atlas_land'],
               transform=myproj)
ax.set_extent(coordinates)

gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                  linewidth=.5, color='gray', alpha=0.5, linestyle='-')
gl.xlabels_top = False
gl.ylabels_right = False


plt.title("Sendero ...")
#plt.savefig(os.path.join(figdir, "contaminants.png"), dpi=300, bbox_inches="tight",
#            facecolor="w", transparent=False)
# plt.show()
plt.close()


# In[ ]:




