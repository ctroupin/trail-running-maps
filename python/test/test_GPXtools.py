import unittest
import GPX_functions

class TestGPX(unittest.TestCase):

    filename = "./test/data/Avion_2019-10-08T17:49:19Z.gpx"

    def test_meanpos(self):
        lonmean, latmean = GPX_functions.compute_mean_pos(filename)
        self.assertEqual(lonmean, 32.)
        self.assertEqual(latmean, 322.2)

if __name__ == '__main__':
    unittest.main()
