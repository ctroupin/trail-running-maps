using FTPClient

@info("Establish connection")
ftp = FTP("ftp://ftp.ifremer.fr/")
cd(ftp, "ifremer/argo/")

@info("Download index file")
download(ftp, "ar_index_global_traj.txt", "/data/Argo/ar_index_global_traj.txt")

@info("Parse index file")
filelist = parse_index_file("ar_index_global_traj.txt")
download(ftp, "dac/aoml/13857/13857_Rtraj.nc", "/data/Argo/13857_Rtraj.nc")
close(ftp)
