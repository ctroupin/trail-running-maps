using FTPClient
using DelimitedFiles

indexfile = "/data/Argo/ar_index_global_traj.txt"
datadir = "/media/ctroupin/My Passport/Argo/Trajectories"
isdir(datadir)

data = readdlm(indexfile, ',', comments=true, comment_char='#');
linetitles = data[1,:];
fileurls = data[2:end,1];
filetimes = data[2:end,end];

# Build the full URLS
ftproot = "ftp://ftp.ifremer.fr/ifremer/argo/dac"
filelist = [joinpath(ftproot, f) for f in fileurls];

for datafile in filelist[:]
    targetfile = joinpath(datadir, basename(datafile))
    if !isfile(targetfile)
        @info("$(datafile) -> $(targetfile)")
        download(datafile, targetfile)
    else
        @debug("File already downloaded")
    end
    #
end


