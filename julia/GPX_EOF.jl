using LinearAlgebra
using PyPlot
include("GPXheat.jl")

datadir = "/data/GPX/Sleeping/500"
if isdir(datadir)
	filelist = GPXheat.get_gpx_list(datadir);
end

nfiles = length(filelist)
# Allocate array
lonarray = Array{Float64, 2}(undef, nfiles, 500)
latarray = Array{Float64, 2}(undef, nfiles, 500)
coordarray = Array{Float64, 2}(undef, nfiles, 1000)

# Loop on files
nlines = 1
for (ii, datafile) in enumerate(filelist)
	global nlines
	@info("Working on $(datafile)")
	lon, lat, d = GPXheat.read_gpx_regex(datafile)
	@info(length(lon))
	if length(lon) >= 500
		lonarray[nlines,:] = lon[1:500]
		latarray[nlines,:] = lat[1:500]
		coordarray[nlines,1:2:end] .= lon[1:500]
		coordarray[nlines,2:2:end] .= lat[1:500]
		nlines += 1
	end
end

lonarray = lonarray[1:nlines-1,:]
latarray = latarray[1:nlines-1,:]
coordarray = coordarray[1:nlines-1,:]

lon_svd = svd(lonarray);
lat_svd = svd(latarray);
coord_svd = svd(coordarray);
