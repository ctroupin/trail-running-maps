# Trail running maps

A _pot-pourri_ of scripts and tools in Bash, Python, Julia and JavaScript to process, transform and display GPS tracks from different devices.

## Data files

The tracks used in this project are stored in [GPX](https://en.wikipedia.org/wiki/GPS_Exchange_Format)  format and have been download from different portals such as:
* [http://www.wikiloc.com/](wikiloc) (manual download),
* [http://movescount.com/](movescount) (automatic script [`movescount-sync`](https://pypi.org/project/movescount-sync/)),
* [https://connect.garmin.com/](Garmin connect) (bulk download).

## What to do with the data?

### Heatmap

Heatmaps provide an simplified representation of the most visited locations. Within the [`DIVAnd`]() module, the function `DIVAnd_heatmap` allows the computation of such heatmaps, based on arrays of positions, as recorded by the device.
Combined tracks           |  Heatmap
:-------------------------:|:-------------------------:
![Gran Canaria heatmap](./images/GCall_heat01.jpg)  |  ![Gran Canaria heatmap](./images/GCall_heat04.jpg)


### Race maps

Not very difficult, just adding the track(s) from famous races on top of a nice satellite image from [Sentinel](https://apps.sentinel-hub.com/eo-browser/) or [NASA Earth](https://worldview.earthdata.nasa.gov/).

### Polor density plot

Just another way to see where we've been running, by creating spatial bins according to distance and angle with respect to the starting position.

### Gridded maps of altitude

As the GPS device is also recording the altitude, we can also use it as an input to the [`DIVA`]() interpolation tool. 
